CREATE TABLE Inventory
(
    id       serial PRIMARY KEY NOT NULL,
    username TEXT               NOT NULL REFERENCES Player (username),
    product  text               NOT NULL REFERENCES Shop (product),
    amount   INT CHECK (amount >= 0),
    UNIQUE (username, product)
);


INSERT INTO Inventory (username, product, amount)
VALUES ('Bob', 'marshmello', 12);
INSERT INTO Inventory (username, product, amount)
VALUES ('Alice', 'marshmello', 23);


